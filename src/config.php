<?php
namespace CSVLoader;

/**
 * Jobs array.
 */
$jobs =
    [
    // 1. Load example.csv file to example_database.example_table and archive the file
    "csv-load-example" => [
        "id" => "1",
        "code" => "csv-load-example",
        "active" => true,

        "Source" => [
            "Folder" => "/home/ftp/inbound",
            "FilePrefix" => "example",
            "ArchiveFolder" => "/home/ftp/inbound/archive",
            "FileExtension" => "csv"
        ],

        "Target" => [
            "Database" => "example_database",
            "Table" => "example_table"
        ]
    ],

   
];

/**
 * Databases array.
 */
$databases =
    [
        "example_database" => [
            "name" => "example_database",
            "Host" => "127.0.0.1",
            "User" => "user",
            "Pass" => "pass"
        ],

    ];