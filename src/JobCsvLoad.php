<?php

namespace CSVLoader;

use PDO;

/**
 * Class JobCsvLoad
 * @package CSVLoader
 */
class JobCsvLoad
{

    /**
     * @var \SimpleXMLElement
     */
    protected $jobConfig;

    /**
     * @var
     */
    protected $pdo;

    /**
     * @var false|string
     */
    protected $sql;

    /**
     * @var
     */
    protected $filesNames;

    /**
     * @var array
     */
    protected $results = array();

    /**
     * @var bool
     */
    protected $debugMode = false;

    private $pdoOptions = [
        PDO::MYSQL_ATTR_LOCAL_INFILE => true
    ];


    /**
     * JobCsvLoad constructor.
     * @param $jobConfig
     */
    public function __construct($jobConfig, $dbConfig)
    {
        $this->jobConfig = $jobConfig;
        $this->dbConfig = $dbConfig;
    }

    /**
     * Callable method for file name filtering.
     *
     * @param string $value
     * @return boolean
     */
    public function doFileFilter($value)
    {
        $pattern = '/' . (string)$this->jobConfig->Source['FilePrefix'] . '/';
        $result = preg_match($pattern, $value);
        return (bool)$result;
    }

    /**
     * Connects to the Database.
     *
     * @return void
     */
    public function connect()
    {
        echo "Connecting to {$this->jobConfig->Target['Database']}" . PHP_EOL;

        try {
            $this->pdo = new \PDO('mysql:host=' . $this->dbConfig->Host . ';dbname=' . $this->dbConfig->name, $this->dbConfig->User, $this->dbConfig->Pass, $this->pdoOptions);
        } catch (\PDOException $ex) {
            print("Error occurred: " . $ex->getMessage());
            die();
        }

        return $this->pdo;
    }

    /**
     * Gets the names of the file available in the source folder.
     *
     * @return void
     */
    public function getFileNames() 
    {
        // Scan Source Folder
        $files = array_diff(scandir($this->jobConfig->Source['Folder']), array(".", ".."));
        // Apply filter on the available items
        return array_filter($files, array($this, 'doFileFilter'));
    }

    /**
     * Load the files in the Database Table and move them in the ArchiveFolder.
     *
     * @return array
     */
    public function load($fileNames)
    {
        $loadScript = \file_get_contents(__DIR__ . '/../sql/' . $this->jobConfig->code . '.sql');
        $results = array();
        $cnt = 1;

        foreach ($fileNames as $file)
        {
            $fileSize = \filesize($this->jobConfig->Source['Folder'] . DIRECTORY_SEPARATOR . $file);

            /* Replace variables in sql load script with current values */
            $sql = str_replace(
                [':file', ':table', ':import_file_name'], 
                [$this->jobConfig->Source['Folder'] . DIRECTORY_SEPARATOR . $file, $this->jobConfig->Target['Table'], $file],
                $loadScript);

            echo "Loading csv file $cnt of ". sizeof($fileNames) . ": $file... ";
            
            if($this->debugMode) {
                echo "Query: " . PHP_EOL . $sql . PHP_EOL;
            }

            $affectedRows = $this->pdo->exec($sql);
            $results[$file]['loaded'] = ($affectedRows ? true : false);
            $results[$file]['affectedRows'] = $affectedRows;

            echo ($results[$file]['affectedRows'] ? $results[$file]['affectedRows'] : 0) . ' rows inserted.' . PHP_EOL;

            /* If load was successful and ArchiveFolder is specified then move the files */
            if ($results[$file]['loaded'] || $fileSize === 0 && (string)$this->jobConfig->Source['ArchiveFolder'])
            {
                echo "Archiving {$file}... ";
                $results[$file]['archived'] = rename($this->jobConfig->Source['Folder'] . '/' . $file, $this->jobConfig->Source['ArchiveFolder'] . '/' . $file);
                echo ($results[$file]['archived'] ? 'ok.' : 'failed.') . PHP_EOL;
            }

            if(!$results[$file]['loaded']) {
                $dbError = $this->pdo->errorInfo();
                echo PHP_EOL . $dbError[2];
            }

            $cnt++;
        }

        return $results;      
    }

    /**
     * Runs the job
     *
     * @return void
     */
    public function run()
    {

        $fileNames = $this->getFileNames();

        /* If there are no files print message and return */
        if(!$fileNames) 
        {
            echo "No {$this->jobConfig->Source['FilePrefix']} .{$this->jobConfig->Source['FileExtension']} files available in {$this->jobConfig->Source['Folder']}" . PHP_EOL . PHP_EOL;
            return;
        }

        $this->connect();
        $this->results = $this->load($fileNames);

        if (!$this->results) 
        {
            echo "No {$this->jobConfig->Source['FilePrefix']} .{$this->jobConfig->Source['FileExtension']} files available in {$this->jobConfig->Source['Folder']}" . PHP_EOL . PHP_EOL;
        }

    }

    /**
     * @return array
     */
    public function getResults()
    {
        return $this->results;
    }



}
