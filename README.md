# csv-loader

PHP application for loading local CSV files into MySQL tables.

## Configuration

The app configuration is in the [config.php](src/config.php).
The config file defines an element in the job array for each CSV file that must be loaded. The job specifies the source file, the file prefix and extension, the archive folder and the target DB name and table and the sql script to be used for loading.
The jobs can be deactivated.

## Running the app

The app is scheduled via crond by specifing the full path to the start script [run.php](run.php), or by launching from the CLI.

The script will look into the config file and execute each CSV loading job.
After loading into the DB the files are either archvied in the specified ArchiveFolder, or, if there is none specified, they will be left in the same folder to be picked up by another job.
