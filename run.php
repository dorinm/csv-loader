<?php
namespace CSVLoader;

require 'src/JobCsvLoad.php';
require 'src/Utils.php';
include 'src/config.php';

$eol = '<br />';
if (Utils::isCLI()) $eol = PHP_EOL;

$jobCode = $argv[1];

echo "[csv-loader-app]" . $eol;

/* Check if the job code was specified */
if (empty($jobCode)) {
    echo "Job code was not specified.";
    exit;
}

echo "Running job: $jobCode " . $eol;

/* Get job config */
$jobConfig = (object) $jobs[$jobCode];
$dbName = $jobConfig->Target["Database"];

/* Get database config for the job */
$dbConfig = (object) $databases[$dbName];

echo "Database: " . $dbConfig->name . $eol;
echo "Server: " . $dbConfig->Host . $eol;

if (!$jobConfig->active) 
{
    echo "Job is inactive. Exiting." . $eol;
    echo " " .$eol;
}

$job = new JobCsvLoad($jobConfig, $dbConfig);
$job->run();

echo " " .$eol;

exit;





