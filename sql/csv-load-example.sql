
LOAD DATA LOCAL INFILE ':file'                 
INTO TABLE :table
FIELDS TERMINATED BY ','
ENCLOSED BY '"'
ESCAPED BY '\\'
LINES TERMINATED BY '\n'
(
    @user,
    @location,
    @lead_id,
    @recording_id,
    @start_time
)
SET
    user = TRIM(@user),
    location = TRIM(@location),
    lead_id = TRIM(@lead_id),
    recording_id = TRIM(@recording_id),
    start_time = TRIM(@start_time),
    import_file_name = ':import_file_name';